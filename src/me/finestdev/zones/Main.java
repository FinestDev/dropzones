package me.finestdev.zones;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener, CommandExecutor {

	int DropZoneTimer, XMax, XMin, ZMax, ZMin;

	static ArrayList<ItemStack> loot = new ArrayList<ItemStack>();
	static ArrayList<String> chests = new ArrayList<String>();

	Location dropPoint;

	public void onEnable(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();

		Bukkit.getPluginManager().registerEvents(this, this);

		// Our variables!

		DropZoneTimer = getConfig().getInt("DropZone.DropTimer");

		XMax = getConfig().getInt("DropZone.Bounds.X.Max");
		XMin = getConfig().getInt("DropZone.Bounds.X.Min");

		ZMax = getConfig().getInt("DropZone.Bounds.Z.Max");
		ZMin = getConfig().getInt("DropZone.Bounds.Z.Min");

		// Setup our items

		for(String s : getConfig().getStringList("DropZone.Items")){
			loot.add(new ItemStack(Material.getMaterial(s), 1));
		}

		dropZoneStart();

	}

	public void onDisable(){
		saveConfig();

		chests.clear();
	}

	public void dropZoneStart(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run(){

				if(Bukkit.getOnlinePlayers().length >= getConfig().getInt("DropZone.Min-Players")){

					// Spawn the crate
					dropCrates();


				}

			}
		},0L, 20L*DropZoneTimer);

	}

	@SuppressWarnings("deprecation")
	public void dropCrates(){
		World w = Bukkit.getWorld(getConfig().getString("DropZone.Bounds.World"));

		final Location spawnPointCenter = generateLocation(w);

		Block c1 = w.getBlockAt(spawnPointCenter.add(1,0,0));
		Block c2 = w.getBlockAt(spawnPointCenter.add(-2,0,0));
		Block c3 = w.getBlockAt(spawnPointCenter.add(1,0,-1));
		Block c4 = w.getBlockAt(spawnPointCenter.add(0,0,2));

		// Here we make our chests!
		c1.getWorld().spawnFallingBlock(c1.getLocation().add(0,50.1,0), Material.WOOD, (byte) 0);
		c2.getWorld().spawnFallingBlock(c2.getLocation().add(0,50.2,0), Material.WOOD, (byte) 0);
		c3.getWorld().spawnFallingBlock(c3.getLocation().add(0,50.3,0), Material.WOOD, (byte) 0);
		c4.getWorld().spawnFallingBlock(c4.getLocation().add(0,50.4,0), Material.WOOD, (byte) 0);

		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("DropZone.Broadcast").replace("%X", 
				spawnPointCenter.getX()+"").replace("%Y", spawnPointCenter.getY()+"").replace("%Z", spawnPointCenter.getZ()+"")));


	}

	@EventHandler
	public void onFall(final EntityChangeBlockEvent e){
		if(e.getEntity() instanceof FallingBlock){
			if(e.getTo() == Material.WOOD){
				e.getEntity().remove();
				new BukkitRunnable(){
					@Override
					public void run(){
						e.getBlock().setType(Material.CHEST);
						Chest chest = (Chest) e.getBlock().getState();
						fillChest(chest);
						chests.add(serialize(chest.getLocation()));
					}
				}.runTaskLater(this, 1L);
			}
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent e){
		if(e.getInventory().getType() == InventoryType.CHEST){
			if(e.getInventory().getHolder() instanceof Chest){
				Chest b = (Chest) e.getInventory().getHolder();

				if(chestCheck(b)){
					if(chests.contains(serialize(b.getLocation()))){
						b.getWorld().getBlockAt(b.getLocation()).setType(Material.AIR);
						chests.remove(serialize(b.getLocation()));
					}
				}
			}
		}
	}

	public Boolean chestCheck(Chest c){
		for(ItemStack item : c.getInventory().getContents()) {
			if(item != null) {
				return false;
			}
		}
		return true;
	}

	public void fillChest(Chest c){
		Random rand = new Random();
		for(int i=0; i < getConfig().getInt("DropZone.ItemCount"); i++){
			c.getInventory().addItem(loot.get(rand.nextInt(loot.size())));
		}
	}

	public Location generateLocation(World w){
		int X = XMin + (int) (Math.random() * ((XMax - XMin) + 1));
		int Y;
		int Z = ZMin = (int) (Math.random() * ((ZMax - ZMin) + 1));

		Location randomPoint = new Location(w, X, 0, Z);
		Y = w.getHighestBlockYAt(randomPoint);

		return new Location(w, X, Y, Z);

	}

	public String serialize(Location l){
		return l.getWorld()+","+(int)l.getX()+","+(int)l.getY()+","+(int)l.getZ();
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		Player p = (Player)sender;
		if(cmd.getName().equalsIgnoreCase("dropzone")){
			if(args.length == 0){
				dropCrates();
			}
		}
		return false;
	}

}
